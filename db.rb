# coding: utf-8

require 'sqlite3'
require 'date'

class DB < SQLite3::Database
  
  def select req
    rows = []
    execute( req ) do |row|
      rows += [row]
    end

    rows
  end
  
  def update req
    execute req
  end
  
  def create
  
  	mois = { 1 => "janvier",
  			 2 => "février",
  			 3 => "mars",
  			 4 => "avril",
  			 5 => "mai",
  			 6 => "juin",
  			 7 => "juillet",
  			 8 => "août",
  			 9 => "septembre",
  			 10 => "octobre",
  			 11 => "novembre",
  			 12 => "décembre"
  		   }
  	
  	update "CREATE TABLE 'cycle' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'mois' TEXT NOT NULL)"
  	update "CREATE TABLE 'datas' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'date' TEXT NOT NULL,'id_cycle' \n
  		INTEGER NOT NULL,'temp' REAL,'max_low_temp' INTEGER NOT NULL DEFAULT (0),'crac_crac' INTEGER NOT NULL DEFAULT (0), \n
  		'blood' INTEGER NOT NULL DEFAULT (0))"
	update "CREATE TABLE 'params' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'name' TEXT NOT NULL,'value' TEXT)"
	
	date = mois[Date.today.month] + " " + Date.today.year.to_s
	update "INSERT INTO cycle (mois) VALUES ('" + date + "')"
	
  end
  
end
