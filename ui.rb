# coding: utf-8

require 'gtk2'

class CycliApp < Gtk::Window
  
    def initialize
        super
                     
        set_title "Cycli"
        signal_connect "destroy" do 
            Gtk.main_quit 
        end
        
        db_file = "temp.db"
        
        create_db = File.exist?(db_file)
               
        @db = DB.new db_file       
        @db.create unless create_db
        @graph = Graph.new @db
                      
        vbox = Gtk::VBox.new false, 2
        vbox.pack_start toolbar, false, false, 0
        vbox.pack_start @graph
        set_default_size 900, 300
        set_window_position Gtk::Window::POS_CENTER
        
        change_cycle 0
        
        #set_icon "./icon.svg"
        
        add vbox        
        
        show_all
    end   
    
    def toolbar
      
        toolbar = Gtk::Toolbar.new
        toolbar.set_toolbar_style Gtk::Toolbar::Style::ICONS
               
        newtb = Gtk::ToolButton.new Gtk::Stock::NEW
        sep = Gtk::SeparatorToolItem.new
        previoustb = Gtk::ToolButton.new Gtk::Stock::GO_BACK
        hometb = Gtk::ToolButton.new Gtk::Stock::HOME
        labelitem = Gtk::ToolItem.new 
        @cyclel = Gtk::Label.new
        labelitem.add @cyclel
        nexttb = Gtk::ToolButton.new Gtk::Stock::GO_FORWARD
        sep2 = Gtk::SeparatorToolItem.new
        detailstb = Gtk::ToolButton.new Gtk::Stock::PAGE_SETUP
        sep3 = Gtk::SeparatorToolItem.new
        quittb = Gtk::ToolButton.new Gtk::Stock::QUIT

        toolbar.insert 0, newtb
        toolbar.insert 1, sep
        toolbar.insert 2, previoustb
        toolbar.insert 3, hometb    
        toolbar.insert 4, nexttb
        toolbar.insert 5, labelitem
        toolbar.insert 6, sep2
        toolbar.insert 7, detailstb
        toolbar.insert 8, sep3
        toolbar.insert 9, quittb
        
        newtb.tooltip_text = "Nouvelle température"
        previoustb.tooltip_text = "Cycle précédent"
        hometb.tooltip_text = "Cycle courant"
        nexttb.tooltip_text = "Cycle suivant"
        detailstb.tooltip_text = "Afficher/Masquer les détails sur le graphique"
        quittb.tooltip_text = "Quitter"
         
        newtb.signal_connect "clicked" do
            popup = ChangeTemp.new @db, @graph
        end
        
        previoustb.signal_connect "clicked" do
            change_cycle -1
        end
        
        hometb.signal_connect "clicked" do
            change_cycle 0
        end
        
        nexttb.signal_connect "clicked" do
            change_cycle 1
        end
         
        detailstb.signal_connect "clicked" do
            @graph.change_point_bool
        end
        
        quittb.signal_connect "clicked" do
            Gtk.main_quit
        end
        
        toolbar
      
    end
    
    def change_cycle m
      if m.eql?(0) then
        res = @db.select("SELECT id, mois FROM cycle ORDER BY id DESC LIMIT 1")
        id = res[0][0].to_i
      else
        id = @graph.get_id_cycle+m
        res = @db.select("SELECT id, mois FROM cycle WHERE id=" + id.to_s)
      end
      
      if res.count>0 then
        @graph.change_cycle id
        @cyclel.text = res[0][1]
      end
    end
       
end
