# coding: utf-8

class Graph < Gtk::DrawingArea

    def initialize db
       
        super()
        
        @id_cycle=3
        @db = db                
        @temp = []
        @temp_min = 0
        @temp_max = 0
        @max_low_temp = 0
        @last_date = Time.new
        @point = true       
        @marge = 40
        @highlight_color = [1, 0, 0]
        
        pos = -1
                
        signal_connect "expose-event" do  
            on_expose
        end
        
        add_events Gdk::Event::BUTTON_PRESS_MASK
        add_events Gdk::Event::POINTER_MOTION_MASK
        
        signal_connect "button-press-event" do |w, event|
          if (event.button==1 and @temp.count>0) then 
            pos = position pointer[0]
            popup = ChangeTemp.new db, self, @temp[pos]
          end
        end
        
        signal_connect "motion-notify-event" do |w, event|
            if (pos!=position(pointer[0]) and @temp.count>0) then
              pos = position pointer[0]
              on_expose pos
            end
        end

    end
    
    def on_expose focus=-1
    
        if !(self.window.nil?) then 
          self.window.clear
          cr = window.create_cairo_context  
          @pas = (@temp.count.eql?(0) ? 1 : (self::allocation.width-@marge*2)/(@temp.count))
          graduations cr, focus
          graph cr, focus
        end

    end 
    
    def graph cr, focus
               
        marge = @marge*1.5
        
        # On trace les lignes
        cr.set_line_width 5
          if !(@temp[0].nil?) then
          cr.move_to marge, temp_to_grad(@temp[0][1])
          @temp.each_index do |i|      
            if i>0 then  
              cr.line_to marge+i*@pas, temp_to_grad(@temp[i][1])    
            end                  
          end
          cr.set_source_rgba 1, 0, 0, 0.40
          cr.set_line_join :ROUND 
          cr.stroke 
        end
        
        # On trace les points et on écrits les températures sur le graphe
        if @point then
          cr.set_source_rgb 0, 0, 1
          cr.set_font_size 12
          @temp.each_index do |i|
            if i==focus then
              cr.set_source_rgb @highlight_color
            else
              cr.set_source_rgb 0, 0, 1
            end
            x = marge+i*@pas
            y = temp_to_grad(@temp[i][1])
            cr.move_to x-10, y-((@temp[i][1].eql?(@temp[i-1][1]) or ( i+1>@temp.count-1 ? true : @temp[i][1].eql?(@temp[i+1][1]) ) )  ? (i%2==0 ? -15 : 5) : 5)
            cr.show_text @temp[i][1].to_s
            cr.arc x, y, 4.0, 0, 2*Math::PI
            cr.fill
            if (@temp[i][2].eql?(1)) then
              pixbuf = Gdk::Pixbuf.new("ressources/love.png")
              cr.set_source_pixbuf(pixbuf, x-7, self::allocation.height-marge)
              cr.paint
            end
            if (@temp[i][5].eql?(1)) then
              cr.set_source_rgba 1, 0, 0, 0.1
              cr.rectangle (i.eql?(0) ? x : x-@pas/2), 0, (i.eql?(0) ? (@pas/2) : @pas), self::allocation.height
              cr.fill
            end
            
          end 
          cr.stroke 
        end     
        
    end
    
    def graduations cr, focus
      
      marge = @marge
      taille_fleche = 6
            
      # On trace l'axe des absices
      cr.set_source_rgba 0, 0, 0, 1
      cr.move_to marge, self::allocation.height-marge
      cr.line_to self::allocation.width-marge/2, self::allocation.height-marge 
      cr.stroke
      cr.move_to self::allocation.width-marge/2, self::allocation.height-marge-taille_fleche
      cr.line_to self::allocation.width-marge/2, self::allocation.height-marge+taille_fleche
      cr.line_to self::allocation.width-marge/2+taille_fleche, self::allocation.height-marge
      cr.line_to self::allocation.width-marge/2, self::allocation.height-marge-taille_fleche
      cr.fill
      @temp.each_index do |i|
        if i==focus then
          cr.set_source_rgb @highlight_color
        else
          cr.set_source_rgb 0, 0, 0
        end
        cr.move_to marge*1.5+i*@pas, self::allocation.height-marge-taille_fleche/2
        cr.line_to marge*1.5+i*@pas, self::allocation.height-marge+taille_fleche/2
        cr.stroke
        # on écrit le jour du cycle
        cr.move_to marge*1.5+i*@pas-(i+1>9 ? 6 :3), self::allocation.height-marge+15
        cr.show_text (i+1).to_s
        # on écrit la date du jour
        d = convert_to_date(@temp[i][3])
        cr.move_to marge*1.5+i*@pas-12, self::allocation.height-marge+(i%2==0 ? 25 : 35)
        cr.set_font_size 8
        cr.set_source_rgb 0, 0, 1 unless i==focus
        cr.show_text d.strftime("%d/%m")
        cr.set_font_size 10
      end
      cr.stroke
            
      # On trace l'axe des ordonnées
      cr.move_to marge, self::allocation.height-marge
      cr.line_to marge, marge 
      cr.set_source_rgb 0, 0, 0   
      cr.stroke
      cr.move_to marge-taille_fleche, marge
      cr.line_to marge+taille_fleche, marge
      cr.line_to marge, marge-taille_fleche
      cr.line_to marge-taille_fleche, marge
      cr.fill
      temp_min = (@temp_min*10).to_i - 1
      temp_max = (@temp_max*10).to_i + 1
      if !(@temp[focus].nil?) then
        (temp_min..temp_max).each do |t|
          y = temp_to_grad(t.to_f/10)
          if (t.to_f/10==@temp[focus][1] and focus>=0) then
            cr.set_source_rgb @highlight_color
          else
            cr.set_source_rgb 0, 0, 0
          end
          cr.move_to marge-taille_fleche/2, y
          cr.line_to marge+taille_fleche/2, y
          cr.stroke
          cr.move_to marge-30, y+3
          cr.show_text (t.to_f/10).to_s
        end
        cr.stroke
      end
      
      
      # On trace la ligne horizontale délimitant les températures basses des températures hautes ---- à automatiser ----
      if (@max_low_temp>@temp_min and @max_low_temp<@temp_max) then
        cr.set_source_rgba 0, 0, 0, 0.5
        cr.move_to @marge, temp_to_grad(@max_low_temp)
        cr.line_to self::allocation.width-@marge+15, temp_to_grad(@max_low_temp) 
        cr.stroke
      end
    end
    
    def temp_to_grad temp_actual
      
      # Pour obtenir la valeur des ordonnées (y) à partir de la température 
      temp_min = @temp_min-0.2
      temp_max = @temp_max+0.2
      h = self::allocation.height
      grad = (temp_actual.nil? ? 0 : h-@marge-((temp_actual-temp_min)*(h-2*@marge))/(temp_max-temp_min))
      grad
    
    end
    
    def change_point_bool
      @point = !@point
      self.on_expose
    end
    
    def load_temp

      req = "SELECT * FROM "
	  req += "(SELECT id, temp, crac_crac, MAX(date) as date, max_low_temp, blood FROM datas WHERE id_cycle=" + (@id_cycle-1).to_s 
	  req += " UNION ALL "
	  req += " SELECT id, temp, crac_crac, date, max_low_temp, blood FROM datas WHERE id_cycle=" + @id_cycle.to_s + ") "
	  req += " T0 "
	  req += " ORDER BY T0.date"
	  #p req
	  @temp = @db.select req
      @temp.delete_at(0) if @temp[0][0].nil?
      @max_low_temp = 0
      minmax = []
      last_date = ""
      @temp.each do |t|
        minmax += [t[1]]
        if t[4].eql?(1) then 
          @max_low_temp=t[1] 
        end
        last_date = t[3]
      end
      @last_date = convert_to_date last_date unless last_date.empty?
      @temp_min = (@temp.count.eql?(0) ? 1 : minmax.min)
      @temp_max = (@temp.count.eql?(0) ? 1 : minmax.max)
      self.on_expose
    end
    
    def position x
      position = ( ((pointer[0]-@marge)/@pas)>@temp.count-1 ? @temp.count-1 : ((pointer[0]-@marge)/@pas) )
      position
    end
    
    def change_cycle id_cycle
      @id_cycle = id_cycle
      load_temp
    end
    
    def get_id_cycle
      @id_cycle
    end
    
    def get_temp
      @temp
    end
    
    def get_last_date
      @last_date
    end
    
    def convert_to_date(str)
      date_array = str.split("-")
      return Date.new(date_array[0].to_i, date_array[1].to_i, date_array[2].to_i)
    end
end
