# coding: utf-8

class ChangeTemp < Gtk::Window
  
    def initialize db, graph, row=nil
        super()
        
        @db = db
        @graph = graph
        @row = row
        @date = (row.nil? ? (@graph.get_last_date + 1) : @graph.convert_to_date(row[3]))
        
        set_title (row.nil? ? "Nouvelle température" : "Modification")
        set_modal true 
        set_keep_above true
        set_decorated true        
               
        set_window_position Gtk::Window::POS_CENTER
        set_border_width 5
        
        vbox = Gtk::VBox.new false, 5
               
        @edit = Gtk::Entry.new       
        @edit.text = row[1].to_s unless row.nil?
        
        add_events(Gdk::Event::KEY_PRESS)
        @edit.signal_connect "key-press-event" do |w, e|
          #p Gdk::Keyval.to_name(e.keyval)
          if (Gdk::Keyval.to_name(e.keyval)=="Return" or Gdk::Keyval.to_name(e.keyval)=="KP_Enter") then
            validate 
          end
        end
        
        hbox1 = Gtk::HBox.new false, 5
        hbox1.add Gtk::Label.new "Température :"
        hbox1.add @edit
               
        if !row.nil? then
          
          delbt = Gtk::Button.new Gtk::Stock::DELETE
          delbt.signal_connect "clicked" do
            dialog = Gtk::Dialog.new( "Suppression ?",
                                     self,
                                     Gtk::Dialog::MODAL,
                                     [ Gtk::Stock::OK, Gtk::Dialog::RESPONSE_ACCEPT ],
                                     [ Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_REJECT] )
           

            # Add the message in a label, and show everything we've added to the dialog.
            dialog.vbox.add(Gtk::Label.new("Etes-vous sûr de vouloir supprimer cette température ?"))
            dialog.show_all
            
            dialog.run do |response|
            
              case response
                when Gtk::Dialog::RESPONSE_ACCEPT
                  puts "suppression"
                  delete
                  destroy
                else
                  puts "annulation"
              end
              dialog.destroy
                
            end
          
          end
          
          hbox1.add delbt
          
        end
             
        vbox.add hbox1
        
        hbox_date = Gtk::HBox.new false, 5
        hbox_date.add Gtk::Label.new "Date :"
        date = Gtk::Label.new @date.strftime("%d/%m/%Y")
        hbox_date.add date
        vbox.add hbox_date
        
        @craccrac = Gtk::CheckButton.new "Crac-Crac ?"    
        @craccrac.set_active (row.nil? ? false : row[2].eql?(1))
        vbox.add @craccrac
        
        @blood = Gtk::CheckButton.new "Saignements ?"    
        @blood.set_active (row.nil? ? false : row[5].eql?(1))
        vbox.add @blood
        
        @maxlowtemp = Gtk::CheckButton.new "Plus haute température basse ?"    
        @maxlowtemp.set_active (row.nil? ? false : row[4].eql?(1))
        vbox.add @maxlowtemp
        
        hbox2 = Gtk::HBox.new true, 5
        validatebt = Gtk::Button.new Gtk::Stock::OK
        cancelbt = Gtk::Button.new Gtk::Stock::CANCEL
        
        validatebt.signal_connect "clicked" do            
          validate 
        end
        
        cancelbt.signal_connect "clicked" do
          destroy
        end
        
        hbox2.add validatebt
        hbox2.add cancelbt
        
        vbox.add hbox2
        
        add vbox
        
        show_all
    end
    
    def validate
      cc = (@craccrac.active? ? 1 : 0)
      mlt = (@maxlowtemp.active? ? 1 : 0)
      blood = (@blood.active? ? 1 : 0)
      if @maxlowtemp.active? then
        # Il ne peut y avoir qu'une seule max_low_temp dans le cycle !
        @db.update ( "UPDATE datas SET max_low_temp=0 WHERE id_cycle=" +  @graph.get_id_cycle.to_s) 
      end
      if @row.nil? then # Si nouvelle temperature
        @db.update ( "INSERT INTO datas (temp, crac_crac, max_low_temp, id_cycle, date, blood) VALUES ('" + @edit.text + "', " + cc.to_s + ", " + mlt.to_s + ", " + @graph.get_id_cycle.to_s + ", '" + @date.strftime("%Y-%m-%d") + "'," + blood.to_s + ")" )
      else # Si modification de température
        @db.update ( "UPDATE datas SET temp='" + @edit.text + "', crac_crac=" + cc.to_s + ", max_low_temp=" + mlt.to_s + ", blood=" + blood.to_s + " WHERE id=" +  @row[0].to_s) 
      end
      @graph.load_temp
      @graph.on_expose
      destroy
    end
    
    def delete
      @db.update("DELETE FROM datas WHERE id=" + @row[0].to_s)
      @graph.load_temp
      @graph.on_expose
    end

end
